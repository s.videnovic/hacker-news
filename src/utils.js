/*
    This is a utility file with miscellaneous helper functions
*/

// clips the number `num` into range [`min`,`max`]
const clip_num = ( num, min, max ) => Math.min( Math.max( num, min ), max )

// calculates the number of subsets (slices)
// example: for total_num_items: 335, maxnum_items_per_slice: 30, the number of slices will be 12
const calculate_num_slices = ( total_num_items, maxnum_items_per_slice ) => {
    const num_slices = Math.ceil( total_num_items / maxnum_items_per_slice )
    return num_slices
}

// Extracts a slice (subset) at position `slice_i` from `all_items` with the max size of `maxnum_items_per_slice`
// examples: 
//   get_items_slice([1,2,3,4,5,6,7], 2, 3) -> [4,5,6]
//   get_items_slice([1,2,3,4,5,6,7], 3, 3) -> [7] 
const get_items_slice = ( all_items, slice_i, maxnum_items_per_slice ) => {
    const total_num_items = all_items.length
    const start_i = clip_num( (slice_i-1)*maxnum_items_per_slice, 0, total_num_items )
	const end_i = clip_num( start_i + maxnum_items_per_slice, 0, total_num_items )
	
	const items_slice = all_items.slice( start_i, end_i )
    return items_slice
}

// simple datetime formater, example output: "22 June 2020, 13:48"
const format_datetime_str = (datetime) => {
    return new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "numeric",
        minute: "numeric"
    }).format(datetime)
}

export { calculate_num_slices, get_items_slice, clip_num, format_datetime_str }