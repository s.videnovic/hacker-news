
const APIs = {
    get_top_stories_url: () => '/v0/topstories.json',
    get_story_data_url: (story_id) => '/v0/item/' + story_id.toString() + '.json',
    get_comment_data_url: (comment_id) => '/v0/item/' + comment_id.toString() + '.json'
}
Object.freeze(APIs)
export default APIs