
const Actions = {
	UPDATE_STORY_IDS: 1,
	UPDATE_STORY_IDS_SLICE: 2,
	UPDATE_STORY_DATA: 3,
	UPDATE_COMMENT_DATA: 4,
	CLEAR_COMMENT_DATA: 5
}
Object.freeze( Actions )

export default Actions