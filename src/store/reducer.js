
import Actions from './actions'
import { calculate_num_slices, get_items_slice, clip_num } from '../utils'

const initialState = {
	app: {
		story_ids: [],
		maxnum_stories_per_page: 30,
		current_page_num: 1,
		total_num_pages: -1
	},
	stories: {
		stories_slice_data: {}
	},
	comments: {}
}

const reducer = (state = initialState, action) => {
	let newState = {}
	
	switch (action.type){

		case Actions.UPDATE_STORY_IDS:
			// saves all top story IDs
			const total_num_pages = calculate_num_slices( 
				action.story_ids.length, state.app.maxnum_stories_per_page 
			)
			newState = {...state}
			newState.app.story_ids = action.story_ids
			newState.app.total_num_pages = total_num_pages
			return newState
		
		case Actions.UPDATE_STORY_IDS_SLICE:
			// inits the current page stories subset
			const page_num = clip_num( action.page_num, 1, state.app.total_num_pages )
			
			const story_ids_slice = get_items_slice(
				state.app.story_ids, page_num, state.app.maxnum_stories_per_page
			)
			const stories_slice_data = {}
			for (const sid of story_ids_slice) {
				stories_slice_data[ sid.toString() ] = {}
			}
			newState = {...state}
			newState.app.current_page_num = page_num
			newState.stories.stories_slice_data = stories_slice_data
			return newState
		
		case Actions.UPDATE_STORY_DATA:
			// fills in the actual story data
			const story_data = {
				id:                 action.story_data.id.toString(),
				author:             action.story_data.by,
				title:              action.story_data.title,
				type:               action.story_data.type,
				url:                action.story_data.url,
				score:              action.story_data.score,
				datetime:           new Date( action.story_data.time*1000 ),
				comment_ids:        ('kids' in action.story_data)? action.story_data.kids : [],
				total_num_comments: ('descendants' in action.story_data)? 
									action.story_data.descendants : []
			}
			newState = {...state}
			newState.stories.stories_slice_data[ story_data.id ] = story_data
			return newState
		
		case Actions.UPDATE_COMMENT_DATA:
			// fills in the actual comment data
			const comment_data = {
				id:          action.comment_data.id.toString(),
				parent:      action.comment_data.parent.toString(),
				by:          action.comment_data.by,
				text:        action.comment_data.text,
				datetime:    new Date( action.comment_data.time*1000 ),
				comment_ids: ('kids' in action.comment_data)? action.comment_data.kids : []
			}
			newState = {...state}
			if (!( action.parent_id in newState.comments )) newState.comments[ action.parent_id ] = {}
			
			newState.comments[ action.parent_id ] = {
				...newState.comments[ action.parent_id ],
				[ comment_data.id ]: comment_data
			}
			return newState
		
		case Actions.CLEAR_COMMENT_DATA:
			// clear comments state every time new page is loaded
			newState = {...state}
			newState.comments = {}
			return newState


		default: break
	}
	return state
}

export default reducer