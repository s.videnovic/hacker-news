import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Axios from 'axios'

import * as serviceWorker from './serviceWorker'
import App from './components/App'
import reducer from './store/reducer'

import './index.scss'

Axios.defaults.baseURL = 'https://hacker-news.firebaseio.com'
const store = createStore( reducer )

const app = (
	<BrowserRouter>
		<Provider store={store}>
			<App />
		</Provider>
	</BrowserRouter>
)
ReactDOM.render( app, document.getElementById('root') )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
