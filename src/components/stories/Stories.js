
import React, { Component } from 'react'
import { connect } from 'react-redux'

import Story from './story/Story'
import Actions from '../../store/actions'

import './Stories.scss'

class Stories extends Component {
	componentDidMount() {
		// every time new page is loaded, clear comments state
		this.props.clearCommentsData()
	}
	render() {
		return (
			<ul className='list-of-ids'>
				{this.props.stories.story_ids_slice.map( (storyID) => (
					<li key={storyID}> <Story id={storyID} ></Story> </li>
				))}
			</ul>
		)
	}
}

const mapStateToProps = state => {
	return {
		stories: {
			story_ids_slice: Object.keys( state.stories.stories_slice_data )
		}
	}
}

const mapDispatchToProps = dispatch => {
	return {
		clearCommentsData: () => dispatch({
			type: Actions.CLEAR_COMMENT_DATA
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Stories) 