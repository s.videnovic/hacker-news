
import React, { Component } from 'react';
import { connect } from 'react-redux'
import Axios from 'axios';

import Actions from '../../../store/actions'
import Comments from './comments/Comments'
import APIs from '../../../APIs'
import { format_datetime_str } from '../../../utils'

import './Story.scss'

class Story extends Component {
	state = {
		error_msg: '',
		show_comments: false,
		loaded: false
	}

	componentDidMount() {
		Axios.get( APIs.get_story_data_url( this.props.id ) )
			.then( response => {
				this.props.updateStoryData( response.data )
				this.setState({ loaded: true })
			})
			.catch( error => {
				const msg = 'Error: '+ error.response.status +' - '+ error.response.statusText
				this.setState({ ...this.state, error_msg: msg })
			})
	}

	toggleComments = (story_id) => {
		this.setState({ show_comments: !this.state.show_comments })
	}

	basic_wrapper = (custom_jsx) => {
		return <div className="story-data"> {custom_jsx} </div>
	}

	render() {
		if (this.state.error_msg){
			return this.basic_wrapper( <p> {this.state.error_msg} </p> )
		}
		if (!this.state.loaded){
			return this.basic_wrapper( <p> Loading... </p> )
		}
		
		const story_data = this.props.stories.stories_slice_data[ this.props.id ]
		
		const dt = format_datetime_str( story_data.datetime )
		const num_comments = story_data.total_num_comments
		const comments_title = ((this.state.show_comments)?'Collapse':'Expand')+' comments'

		return this.basic_wrapper((
			<React.Fragment>
				<a href={story_data.url} rel='noopener noreferrer' target="_blank"> 
					{story_data.title} 
				</a>
				<div>
					<span> <label>Posted by:</label> {story_data.author} </span>
					<span> <label>Score:</label> {story_data.score} </span>
					<span> <label>Date:</label> {dt} </span>
					<span className='comment-openner'
							title={comments_title}
							onClick={() => this.toggleComments(this.props.id)}> 
						<label className='comment-openner'>comments: </label>{num_comments}
					</span>
				</div>
				{(this.state.show_comments) ?
				<Comments parent_id={this.props.id} parent_type='story' ></Comments> : null }
			</React.Fragment>
		))

	}
}

const mapStateToProps = state => {
	return {
		stories:{
			stories_slice_data: state.stories.stories_slice_data
		} 
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateStoryData: (story_data) => dispatch({
			type: Actions.UPDATE_STORY_DATA, story_data: story_data
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Story)