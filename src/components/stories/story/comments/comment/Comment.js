
import React, { Component } from 'react'

import Comments from '../Comments'
import { format_datetime_str } from '../../../../../utils'

import './Comment.scss'

class Comment extends Component {

	htmlDecode = (input) => {
		// safely decodes html content received as comment text
		const doc = new DOMParser().parseFromString(input, "text/html");
		return doc.documentElement.textContent;
	}

	render() {
		if (!this.props.data.by) return null
		
		const dt = format_datetime_str( this.props.data.datetime )
		
		return (
			<div className='comment-container'>
				<div>
					<span> <label> by: </label> {this.props.data.by} </span>
					<span> <label> date: </label> {dt} </span>
				</div>
				<p className='comment-text'> {this.htmlDecode(this.props.data.text)} </p>
				<Comments parent_id={this.props.data.id} parent_type='comment' ></Comments>
			</div>
		) 
	}
}

export default Comment