
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Axios from 'axios';

import Actions from '../../../../store/actions'
import Comment from './comment/Comment'
import APIs from '../../../../APIs';

import './Comments.scss'

class Comments extends Component {
	state = {
		error_msg: '',
		loaded: false,
		comment_counter: 0 // used as a trigger for the `loaded` flag
	}

	fetchComments = (comment_ids) => {
		if (comment_ids.length === 0){
			// handles the case where there are 0 comments, 
			// otherwise it would stay "Loading..." indefinitely
			this.setState({ loaded: true })
		}
		for (let comment_id of comment_ids){
			Axios.get( APIs.get_comment_data_url( comment_id ) )
				.then( response => {
					this.props.updateCommentsData( 
						this.props.parent_id.toString(), response.data 
					)
					this.setState({ comment_counter: this.state.comment_counter + 1 })
					if (comment_ids.length === this.state.comment_counter){
						this.setState({ loaded: true })
					}
				})
				.catch(error => {
					const msg = 'Error: '+ error.response.status +' - '+ error.response.statusText
					this.setState({ ...this.state, error_msg: msg })
				})
		}
	}
		
	componentDidMount() {
		let comment_ids = []
		
		if (this.props.parent_type === "story"){
			comment_ids = this.props.stories.stories_slice_data[ this.props.parent_id ].comment_ids
			this.fetchComments( comment_ids )
		}
		else if (this.props.parent_type === "comment"){
			Axios.get( APIs.get_comment_data_url( this.props.parent_id ) )
				.then( response => {
					comment_ids = ('kids' in response.data)? response.data.kids : [] 
					this.fetchComments( comment_ids )
				})
				.catch(error => {
					const msg = 'Error: '+ error.response.status +' - '+ error.response.statusText
					this.setState({ ...this.state, error_msg: msg })
				})
		}
	}

	basic_wrapper = (custom_jsx) => {
		return <div className='comments-container'> {custom_jsx} </div>
	}

	render() {
		if (this.state.error_msg){
			return this.basic_wrapper( <p> {this.state.error_msg} </p> )
		}
		if (!this.state.loaded){
			return this.basic_wrapper( <p> Loading... </p> )
		}

		let comments_data = this.props.comments[ this.props.parent_id.toString() ]
		if (!comments_data) return null

		return this.basic_wrapper((
			<React.Fragment>
				{Object.keys( comments_data ).map( id => (
					<Comment key={id} data={comments_data[id]} ></Comment>
				))}
			</React.Fragment>
		))
	
	}
}

const mapStateToProps = state => {
	return {
		stories: {
			stories_slice_data: state.stories.stories_slice_data
		},
		comments: state.comments
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateCommentsData: (parent_id, comment_data) => dispatch({
			type: Actions.UPDATE_COMMENT_DATA, parent_id: parent_id, comment_data: comment_data
		})
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(Comments)