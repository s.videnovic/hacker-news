
import React, { Component } from 'react'
import Axios from 'axios';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import Stories from './stories/Stories'
import Actions from '../store/actions'
import APIs from '../APIs'

import './App.scss'

class App extends Component {
	state = {
		error_msg: '',
		loaded: false
	}

	componentDidMount() {
		Axios.get( APIs.get_top_stories_url() )
			.then( response => {
				this.props.updateStoryIDs( response.data )
				const url_page_num = this.parseURLforPageNum()
				this.props.gotoStoriesPage( url_page_num )
				this.setState({ loaded: true })
			})
			.catch(error => {
				const msg = 'Error: '+ error.response.status +' - '+ error.response.statusText
				this.setState({ ...this.state, error_msg: msg })
			})
	}

	gotoStoriesPage = ( page_num ) => {
		this.props.gotoStoriesPage( page_num )
		this.props.history.push({ pathname:'/page/'+page_num })
	}

	parseURLforPageNum = () => {
		const match = window.location.href.match( /[/]page[/](\d+)/ )
		let page_num = 1
		if (match){
			const param = match[1]
			if (!isNaN(param)) page_num = parseInt(param)
		} 
		return page_num
	}

	basic_wrapper = (custom_jsx) => {
		return (
			<div className="App">
				<h1> Hacker News </h1>
				{custom_jsx}
				<p> 2020 - Stefan Videnović </p>
			</div>
		)
	}

	render() {
		if (this.state.error_msg){
			return this.basic_wrapper( <p> {this.state.error_msg} </p> )
		}
		if (!this.state.loaded){
			return this.basic_wrapper( <p> loading... </p> )
		}

		const page_nums = [...Array(this.props.app.total_num_pages).keys()].map( item => item+1 )
		const page_nums_list = (
			<ul className='list-of-page-nums'> 
				<li className='page-label'> Page: </li> 
				{page_nums.map( page_num => {
					const LinkClass = ( page_num === this.props.app.current_page_num )
										? "CompLink-active" : "CompLink"
					return (
						<li 
							key={page_num} 
							className={LinkClass} 
							onClick={() => this.gotoStoriesPage(page_num)} > {page_num} </li>
					)
				})}
			</ul>
		)
		return this.basic_wrapper(
			<React.Fragment>
				{page_nums_list}
				<Stories></Stories>
				{page_nums_list}
			</React.Fragment>
		)
		
	}
}

const mapStateToProps = state => {
	return {
		app: {
			maxnum_stories_per_page: state.app.maxnum_stories_per_page,
			current_page_num: state.app.current_page_num,
			total_num_pages: state.app.total_num_pages
		}
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateStoryIDs: (story_ids) => dispatch({
			type: Actions.UPDATE_STORY_IDS, story_ids: story_ids
		}),
		gotoStoriesPage: (page_num) => dispatch({
			type: Actions.UPDATE_STORY_IDS_SLICE, page_num: page_num,
		})
	}
}

export default withRouter( connect( mapStateToProps, mapDispatchToProps )(App) )
