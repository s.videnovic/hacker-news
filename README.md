

# Hacker News

This a demo project, loose recreation of: https://news.ycombinator.com/ 
![example](public/hacker_news_demo.gif)



### Setting up the environment

```bash
cd hacker-news
yarn install && yarn add node-sass redux react-redux
```

If you don't have `yarn` simply install it with `npm install yarn` I only used it because of `node-sass` package, as it was the easiest way to have modular SASS styling.

### Running the app

```
yarn start
```



### TODO

This project was done in a rush (in effectively 2 days), since I was learning all these new front-end technologies before and while implementing it, so now I realize that there are things that could be improved:

- **Comment loading** - currently all comments (related to a single Story) are loaded at once when expanded, this should be changed to load only immediate comments (tree level 1) first and the rest of them should be loaded manually when expanding subcomments. The reason for this is that once there are say 500 comments total, it takes a few minutes to get them all while the app kinda freezes.
- **REDUX state** - it is currently being handled by a single reducer, which is fine for this application, but for a larger one it should be split into multiple reducers.
- **Maybe use more modern approaches** - like: `async/await` instead of `Axios.get(...).then(...).catch(...)` and Functional components with React hooks, instead of class-based components.
- **Optimizations** - currently components are being rerendered even if they don't need to be, this could be fixed with Lifecycle hooks, that would check if a complete rerender is really necessary.